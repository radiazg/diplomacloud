from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.
def cursos(request):
    cursos_json = { 'cursos':[
            {'id':'DS001',
            'name': 'Big Data Analytics',
            'start': '20-07-2021'},
            {'id':'DS002',
            'name': 'Analytics: Essential tools and methods',
            'start': '27-07-2021'},
            {'id':'DS003',
            'name': 'Python for Data Analytics',
            'start': '20-07-2021'},
            {'id':'CS004',
            'name': 'Programming for Data Science',
            'start': '27-07-2021'},
            {'id':'CS005','name': 'Advance Programming in C++',
            'start': '20-07-2021'}
        ]
    }
    return JsonResponse(cursos_json)