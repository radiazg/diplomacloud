from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.
# Create your views here.
def cursos(request):
    cursos_json = { 'cursos': [
        {'name': 'Introduction to Java Programming',
        'start': '20-07-2021',
        'status': 'Approved'},
        {'name': 'Computing in Python: Data structures',
        'start': '27-07-2021',
        'status': 'Approved'},
        {'name': 'Object-Oriented Programming',
        'start': '20-07-2021',
        'status': 'Ongoing'}
        ]
    }
    return JsonResponse(cursos_json)