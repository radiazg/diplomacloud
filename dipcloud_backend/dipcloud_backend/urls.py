"""dipcloud_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from api_web import views as api_web_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('projects', api_web_views.projects, name="projects"),
    path('users', api_web_views.users, name="users"),
    path('vmachines', api_web_views.vmachines, name="vmachines"),
    path('users/<str:user_id>', api_web_views.users_detail, name="users_detail"),
]
