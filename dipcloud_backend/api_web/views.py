from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt

domain_info = {
    'domain': {
        'id': 'def42f523',
        'name': 'dip-cloud-pucp',
        'description': 'Domain for Diploma Cloud environments'
    },
    'projects': [
        { 'id':'d54bd36b', 'name': 'dipcloud_1', 'domain': 'dip-cloud-pucp'},
        { 'id':'96c904f0', 'name': 'dipcloud_2', 'domain': 'dip-cloud-pucp'},
        { 'id':'f3334aa3', 'name': 'dipcloud_3', 'domain': 'dip-cloud-pucp'},
        { 'id':'2c0822e2', 'name': 'dipcloud_4', 'domain': 'dip-cloud-pucp'},
        { 'id':'df6e8940', 'name': 'dipcloud_5', 'domain': 'dip-cloud-pucp'},
        { 'id':'16399f2a', 'name': 'dipcloud_6', 'domain': 'dip-cloud-pucp'},
        { 'id':'a72a3d3b', 'name': 'dipcloud_7', 'domain': 'dip-cloud-pucp'},
    ]
}

users_info = {
    'user001': { 'name': 'Cornifer', 'email': 'cornifer@cornifer.mail.com', 'project_id':  'd54bd36b'},
    'user002': { 'name': 'Hornet', 'email': 'hornet@hornet.mail.com', 'project_id':  '96c904f0'},
    'user003': { 'name': 'Quirrel', 'email': 'quirrel@quirrel.mail.com', 'project_id':  'f3334aa3'},
    'user004': { 'name': 'Tiso', 'email': 'tiso@mail.com', 'project_id':  '2c0822e2'},
    'user005': { 'name': 'Zotel', 'email': 'zotel@mail.com', 'project_id':  'df6e8940'}
}

users_details = {
    'user001': { 'name': 'Cornifer', 'email': 'cornifer@cornifer.mail.com', 'project_id':  'd54bd36b', 'status': 'active', 'role': 'supervisor'},
    'user002': { 'name': 'Hornet', 'email': 'hornet@hornet.mail.com', 'project_id':  '96c904f0', 'status': 'active', 'role': 'user'},
    'user003': { 'name': 'Quirrel', 'email': 'quirrel@quirrel.mail.com', 'project_id':  'f3334aa3', 'status': 'active', 'role': 'user'},
    'user004': { 'name': 'Tiso', 'email': 'tiso@mail.com', 'project_id':  '2c0822e2', 'status': 'active', 'role': 'user'},
    'user005': { 'name': 'Zotel', 'email': 'zotel@mail.com', 'project_id':  'df6e8940', 'status': 'active', 'role': 'user'},
}

vmachines_info = {
    'd5824f13f0c1' : { 'project_id': 'd54bd36b', 'hostname': 'master', 'ip_address': '10.20.12.11','status':'RUNNING'},
    'd5824f13f0c2' : { 'project_id': '96c904f0', 'hostname': 'master', 'ip_address': '10.20.12.12','status':'RUNNING'},
    'd5824f13f0c3' : { 'project_id': 'f3334aa3', 'hostname': 'master', 'ip_address': '10.20.12.13','status':'RUNNING'},
    'd5824f13f0c4' : { 'project_id': '2c0822e2', 'hostname': 'master', 'ip_address': '10.20.12.14','status':'FAILED'},
    'd5824f13f0c5' : { 'project_id': 'df6e8940', 'hostname': 'master', 'ip_address': '10.20.12.15','status':'STOPPED'},
    '081edeeee1a1' : { 'project_id': 'd54bd36b', 'hostname': 'worker-1', 'ip_address': 'None', 'status':'RUNNING'},
    '081edeeee1a2' : { 'project_id': 'd54bd36b', 'hostname': 'worker-2', 'ip_address': 'None', 'status':'STOPPED'},
    '081edeeee1a3' : { 'project_id': '96c904f0', 'hostname': 'worker-1', 'ip_address': 'None', 'status':'RUNNING'},
    '081edeeee1a4' : { 'project_id': '96c904f0', 'hostname': 'worker-2', 'ip_address': 'None', 'status':'RUNNING'},
    '081edeeee1a5' : { 'project_id': 'f3334aa3', 'hostname': 'worker-1', 'ip_address': 'None', 'status':'RUNNING'},
    '081edeeee1a6' : { 'project_id': 'f3334aa3', 'hostname': 'worker-2', 'ip_address': 'None', 'status':'FAILED'},
    '081edeeee1a7' : { 'project_id': '2c0822e2', 'hostname': 'worker-1', 'ip_address': 'None', 'status':'RUNNING'},
    '081edeeee1a8' : { 'project_id': '2c0822e2', 'hostname': 'worker-2', 'ip_address': 'None', 'status':'RUNNING'},
    '081edeeee1a9' : { 'project_id': 'df6e8940', 'hostname': 'worker-1', 'ip_address': 'None', 'status':'FAILED'},
    '081edeeee1a0' : { 'project_id': 'df6e8940', 'hostname': 'worker-2', 'ip_address': 'None', 'status':'RUNNING'},
}

# Create your views here.
@csrf_exempt
def projects(request):
    if request.method == "GET":
        return JsonResponse(domain_info)
    elif request.method == "POST":
        response = HttpResponse()
        response.status_code = 404
        response.headers['Message'] = "The resource is not available"
        return response
    else:
        return HttpResponse(status = 404)

@csrf_exempt
def users(request):
    if request.method == "GET":
        return JsonResponse(users_info)
    elif request.method == "POST":
        request_username = request.headers.get('Username')
        if( not request_username == None):
            user_name = request.POST.get('user_name', None)
            user_email = request.POST.get('user_email', None)
            response = HttpResponse()
            if not (user_name is None or user_email is None):
                response.status_code = 200
                response.headers['Message'] = "Your username was created successfully "
            else:
                response.status_code = 400
                response.headers['Message'] = "Bad Request: User data not found: Please include user_name and user_email info in your request"

        else:
            response = HttpResponse()
            response.status_code = 404
            response.headers['Message'] = "The username was not found in the headers"
        return response
    else:
        return HttpResponse(status_code = 404)

@csrf_exempt
def vmachines(request):
    # return JsonResponse(vmachines_info)
    if request.method == "GET":
        return JsonResponse(vmachines_info)
    elif request.method == "POST":
        response = HttpResponse()
        response.status_code = 404
        response.headers['Message'] = "The resource is not available"
        return response
    else:
        return HttpResponse(status_code = 404)

@csrf_exempt
def users_detail(request, user_id):
    if request.method == "GET":
        if(user_id in users_info):
            user_detail = users_details[user_id]
        else:
            user_detail = {'status': 'Not Found'}
        return JsonResponse(user_detail)
    elif request.method == "POST":
        response = HttpResponse()
        response.status_code = 404
        response.headers['Message'] = "The resource is not available"
        return response
    else:
        return HttpResponse(status_code = 404)
