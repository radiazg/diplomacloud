from django.shortcuts import render
import requests

# Create your views here.
def home(request):
    cursos_r = requests.get('http://localhost:8080/my-courses/')
    cursos = cursos_r.json()['cursos']
    matricula_r = requests.get('http://localhost:8090/avilable-courses/')
    matricula = matricula_r.json()['cursos']
    return render(request,"core/home.html",  {'matricula':matricula, 'cursos': cursos})